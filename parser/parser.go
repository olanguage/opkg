package parser

import (
	"bufio"
	"log"
	"os"
	"gitlab.com/olanguage/olang/lexer"
	"gitlab.com/olanguage/olang/token"
)

type Parser struct {
	l      *lexer.Lexer
	errors []string

	curToken  token.Token
	peekToken token.Token

}

func New(l *lexer.Lexer) *Parser {
	p := &Parser{
		l:      l,
		errors: []string{},
	}

	// Read two tokens, so curToken and peekToken are both set
	p.nextToken()
	p.nextToken()

	return p
}

func (p *Parser) Errors() []string {
	return p.errors
}

func (p *Parser) nextToken() {
	p.curToken = p.peekToken
	p.peekToken = p.l.NextToken()
}

func (p *Parser) prevToken(prev int) {
	p.curToken = p.peekToken
	p.peekToken = p.l.PrevToken(prev)
}

func (p *Parser) ParseLib(path string) string {
	result := ""

	for p.curToken.Type != token.EOF {
		switch p.curToken.Type {
		case token.LOAD:
			p.nextToken()
			filename := p.curToken.Literal
			file, err := os.Open(path+filename)
			if err != nil {
				log.Fatal(err)
			}
			defer file.Close()

			scanner := bufio.NewScanner(file)

			for scanner.Scan() {
				line := scanner.Text()
				result += line+"\n"
			}
			p.prevToken(1)
			break
		case token.STRING:
			result += "\""+p.curToken.Literal+"\""
			p.nextToken()
			break
		case token.SEMICOLON:
			result += token.SEMICOLON
			p.nextToken()
			break
		default:
			result += " "+p.curToken.Literal
			p.nextToken()
			break
		}
	}

	if p.curToken.Type == token.EOF{
		result += "\n"
	}


	return result
}