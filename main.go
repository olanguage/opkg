package main

import (
	"github.com/urfave/cli"
	"os"
	"sort"
	"gitlab.com/olanguage/opkg/olpfile"
)

func main(){
	app := &cli.App{
		Name: "opkg",
		Usage: "Olang package manager",
		Version: "1.0",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:  "output, o",
				Usage: "Output File",
			},
		},
		Commands: []*cli.Command{
			{
				Name:    "generate",
				Aliases: []string{"g"},
				Usage:   "Install all depends",
				Action:  func(c *cli.Context) error {
					olp := olpfile.New("olpfile.json", "vendor")
					olp.CreateFile()
					return nil
				},
			},
			{
				Name:    "install",
				Aliases: []string{"i"},
				Usage:   "Install all depends",
				Action:  func(c *cli.Context) error {
					olp := olpfile.New("olpfile.json", "vendor")
					olp.Install()
					return nil
				},
			},
			{
				Name:    "build",
				Aliases: []string{"b"},
				Usage:   "Build project",
				Action:  func(c *cli.Context) error {
					olp := olpfile.New("olpfile.json", "vendor")
	
					if olpfile.Exists("vendor/olp.ola"){
						parsedOlp := olp.Parse()
	
						olp.CompileProject(parsedOlp.Main, parsedOlp.Compiled+"/", parsedOlp.Name)
					}
					return nil
				},
			},
			{
				Name:    "get",
				Aliases: []string{"g"},
				Usage:   "Get sources to this folder",
				Action:  func(c *cli.Context) error {
					olp := olpfile.New("olpfile.json", "vendor")
	
					parsed := olp.Parse()
	
					userEnter := olpfile.Require{Name: c.Args().Get(0), Src: c.Args().Get(1)}
	
					parsed.Requires = append(parsed.Requires, userEnter)
	
					olp.WriteOlpFile(parsed)
					olp.Install()
	
					return nil
				},
			},
			{
				Name:    "job",
				Aliases: []string{"j"},
				Usage:   "Run jobs in project",
				Action:  func(c *cli.Context) error {
					olp := olpfile.New("olpfile.json", "vendor")
					jobname := c.Args().First();
					olp.JobRun(jobname)
					return nil
				},
			},
			{
				Name:    "watch",
				Aliases: []string{"w"},
				Usage:   "Watch Changed Files and Create JS Bundle",
				Action:  func(c *cli.Context) error {
					olp := olpfile.New("olpfile.json", "vendor")
					parsed := olp.Parse()
					olp.Watch(parsed.WatchList.Folder, parsed.WatchList.Output)
	
					return nil
				},
			},
		},
	}

	sort.Sort(cli.CommandsByName(app.Commands))

	app.Run(os.Args)
}